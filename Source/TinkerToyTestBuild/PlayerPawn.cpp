// Fill out your copyright notice in the Description page of Project Settings.

#include "TinkerToyTestBuild.h"
#include "PlayerPawn.h"


// Sets default values
APlayerPawn::APlayerPawn()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	atStart = true;
	destIsSet = false;
	foundMyHome = false;
	goPath = false;

	firstLocation = GetActorLocation();
	CurrentProjectedLoc = GetActorLocation();

	//make the sphere object the new root component
	root = CreateDefaultSubobject<UCapsuleComponent>(TEXT("ROOT"));
	RootComponent = root;
	root->RelativeLocation = FVector(0.0f, 0.0f, 0.0f);
	root->InitCapsuleSize(18.0f, 0.5f);

	// Create the sprite for the ship.
	m_personVisual = CreateDefaultSubobject<UPaperSpriteComponent>(TEXT("PersonVisual"));
	m_personVisual->RelativeLocation = FVector(0.0f, 0.0f, 0.0f);
	m_personVisual->SetSprite(ConstructorHelpers::FObjectFinder<UPaperSprite>
		(TEXT("/Game/Sprites/mainCharacter_Sprite.mainCharacter_Sprite")).Object);

	

	// Setup movement.
	m_movementComponent = CreateDefaultSubobject<UFloatingPawnMovement>(TEXT("Movement"));
	m_movementComponent->SetUpdatedComponent(RootComponent);
	m_movementComponent->MaxSpeed = 500.0f;
	m_movementComponent->Acceleration = m_movementComponent->MaxSpeed * 5;
	m_movementComponent->Deceleration = m_movementComponent->MaxSpeed * 5;
	
}

// Called when the game starts or when spawned
void APlayerPawn::BeginPlay()
{
	Super::BeginPlay();
	
	// Iterate over all Actors in the world and save their locations
	for (TActorIterator<AActor> ActorItr(GetWorld()); ActorItr; ++ActorItr) {
		AActor *Mesh = *ActorItr;
		// If Actor is a �PathNode�, display location on screen.
		if (ActorItr->GetName().Contains("PathNode_BP")) {
			FVector nodeLocation = ActorItr->GetActorLocation();
			NodeLocArray.Add(nodeLocation);
		}
	}

	//float sizeOfAB = (NodeLocArray[0] - NodeLocArray[1]).Size();
	//UE_LOG(LogTemp, Log, TEXT("A is at: %s"), *NodeLocArray[0].ToString());
	//UE_LOG(LogTemp, Log, TEXT("B is at: %s"), *NodeLocArray[1].ToString());
	//UE_LOG(LogTemp, Log, TEXT("Size of AB is: %f"), sizeOfAB);

	if (GetWorld()) {
		APlayerController* MyController = GetWorld()->GetFirstPlayerController();

		MyController->bShowMouseCursor = true;
		MyController->bEnableClickEvents = true;
		MyController->bEnableMouseOverEvents = true;
	}
	
	
}

// Called every frame
void APlayerPawn::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );

	if (isHit) {
		PlayerHP -= 10.0f;
	}

	if (PlayerHP <= 0) {
		UGameplayStatics::OpenLevel(GetWorld(), "Credits");
	}


}

// Called to bind functionality to input
void APlayerPawn::SetupPlayerInputComponent(class UInputComponent* InputComponent)
{
	Super::SetupPlayerInputComponent(InputComponent);


}

FVector APlayerPawn::FindClosestNodeStart(FVector fromHere, FVector toThere) {
	int smallestDistance = 10000;
	FVector smallestVector;
	UE_LOG(LogTemp, Log, TEXT("Inside find START closest node"));
	for (int a = 0; a < NodeLocArray.Num(); a = a++) {
		FVector LocNode = NodeLocArray[a];
		float sizeOfAB = (LocNode - fromHere).Size();  //distance between us and this node from the array
		if (sizeOfAB<smallestDistance && sizeOfAB != 0) {
			smallestDistance = sizeOfAB;
			smallestVector = LocNode;
		}
	}
	UE_LOG(LogTemp, Log, TEXT("Returned first start node: %s"), *smallestVector.ToString());
	return smallestVector;
}

FVector APlayerPawn::FindClosestNode() {
	int smallestDistance = 10000;
	FVector smallestVector;
	UE_LOG(LogTemp, Log, TEXT("Inside find closest node"));
	for (int a = 0; a < NodeLocArray.Num(); a = a++) {
		FVector LocNode = NodeLocArray[a];
		float sizeOfAB = (LocNode - CurrentProjectedLoc).Size();  //distance between us and this node from the array
		if (sizeOfAB < 120) {
			CurrentConnectionList.Add(LocNode);
		}
	}

	for (int b = 0; b < CurrentConnectionList.Num(); b = b++) {
		FVector ConnectionNode = CurrentConnectionList[b];
		float sizeOfBC = (ConnectionNode - NewDestination).Size();
		if (sizeOfBC<smallestDistance) {
			smallestDistance = sizeOfBC;
			smallestVector = ConnectionNode;
		}
	}
	UE_LOG(LogTemp, Log, TEXT("Returned smallest vector: %s"), *smallestVector.ToString());
	return smallestVector;

}

void APlayerPawn::MakePathNode() {
	//GEngine->AddOnScreenDebugMessage(-1, 50.f, FColor::Red, TEXT("Inside make path"));
	UE_LOG(LogTemp, Log, TEXT("Inside make path"));
	FVector newClosest;

	if (atStart == false) {
		newClosest = FindClosestNode();
	}

	if (atStart == true) {
		newClosest = FindClosestNodeStart(CurrentProjectedLoc, NewDestination);
		atStart = false;
	}

	UE_LOG(LogTemp, Log, TEXT("Adding new closest"));
	ourPath.Add(newClosest);
	CurrentProjectedLoc = newClosest;
	UE_LOG(LogTemp, Log, TEXT("Current projected loc is: %s"), *CurrentProjectedLoc.ToString());
	UE_LOG(LogTemp, Log, TEXT("Destination is: %s"), *NewDestination.ToString());
	//GEngine->AddOnScreenDebugMessage(-1, 50.f, FColor::Red, TEXT("Current projected loc is: " + CurrentProjectedLoc.ToString()));
	//GEngine->AddOnScreenDebugMessage(-1, 50.f, FColor::Red, TEXT("Destination is: " + NewDestination.ToString()));
}
