// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "PlayerPawn.h"
#include "PaperSpriteComponent.h"
#include "PathNodeActor.generated.h"

UCLASS()
class TINKERTOYTESTBUILD_API APathNodeActor : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	APathNodeActor();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;


	// Is selected? Boolean
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		bool isSelected;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		TArray<FVector> connectToLocations;
	
	UFUNCTION()
		TArray<FVector> getConnectToLocations();

private:
	UPROPERTY(VisibleDefaultsOnly)
		UPaperSpriteComponent *m_pathVisual;

};
