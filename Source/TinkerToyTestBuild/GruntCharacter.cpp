// Fill out your copyright notice in the Description page of Project Settings.
//Author: Marco Duran 

#include "TinkerToyTestBuild.h"
#include "GruntCharacter.h"
#include "Casts.h"

#include "EngineUtils.h"


// Sets default values
AGruntCharacter::AGruntCharacter()
{
	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	//GetMesh()->SetVisibility(false, false);
	/*******************IdleRight**************************/
	m_grunt_idle_right = CreateDefaultSubobject<UPaperFlipbookComponent>(TEXT("GruntIdleRightVisual"));
	//m_grunt_idle_right->AttachTo(GetCapsuleComponent());
	m_grunt_idle_right->AttachParent = GetCapsuleComponent();
	m_grunt_idle_right->RelativeLocation = FVector(0.0f, 0.0f, 0.0f);
	//m_grunt_idle_right->RelativeScale3D = FVector(1.5f, 1.5f, 1.5f);
	m_grunt_idle_right->SetFlipbook(ConstructorHelpers::FObjectFinder<UPaperFlipbook>
		(TEXT("/Game/Animations/Grunt/grunt_idle_right.grunt_idle_right")).Object);
	m_grunt_idle_right->Stop();
	m_grunt_idle_right->SetVisibility(false, false);
	currentFlipbook = m_grunt_idle_right; //sets the initial current flipbook
    
	/********************Idle Left***************************/
	//creating references to all of the animations
	m_grunt_idle_left = CreateDefaultSubobject<UPaperFlipbookComponent>(TEXT("GruntIdleLeftVisual"));
	//m_grunt_idle_left->AttachTo(GetCapsuleComponent());
	m_grunt_idle_left->AttachParent = GetCapsuleComponent();
	m_grunt_idle_left->RelativeLocation = FVector(0.0f, 0.0f, 0.0f);
	//m_grunt_idle_left->RelativeScale3D = FVector(1.5f, 1.5f, 1.5f);
	m_grunt_idle_left->SetFlipbook(ConstructorHelpers::FObjectFinder<UPaperFlipbook>
	(TEXT("/Game/Animations/Grunt/grunt_idle_left.grunt_idle_left")).Object);
	m_grunt_idle_left->Stop();
	m_grunt_idle_left->SetVisibility(false, false);

	/********************Idle Up***************************/
	m_grunt_idle_up = CreateDefaultSubobject<UPaperFlipbookComponent>(TEXT("GruntIdleUpVisual"));
	//m_grunt_idle_up->AttachTo(GetCapsuleComponent());
	m_grunt_idle_up->AttachParent = GetCapsuleComponent();
	m_grunt_idle_up->RelativeLocation = FVector(0.0f, 0.0f, 0.0f);
	m_grunt_idle_up->SetFlipbook(ConstructorHelpers::FObjectFinder<UPaperFlipbook>
		(TEXT("/Game/Animations/Grunt/grunt_idle_up.grunt_idle_up")).Object);
	m_grunt_idle_up->Stop();
	m_grunt_idle_up->SetVisibility(false, false);

	/********************Idle Down***************************/
	m_grunt_idle_down = CreateDefaultSubobject<UPaperFlipbookComponent>(TEXT("GruntIdleDownVisual"));
	//m_grunt_idle_down->AttachTo(GetCapsuleComponent());
	m_grunt_idle_down->AttachParent = GetCapsuleComponent();
	m_grunt_idle_down->RelativeLocation = FVector(0.0f, 0.0f, 0.0f);
	m_grunt_idle_down->SetFlipbook(ConstructorHelpers::FObjectFinder<UPaperFlipbook>
		(TEXT("/Game/Animations/Grunt/grunt_idle_down.grunt_idle_down")).Object);
	m_grunt_idle_up->Stop();
	m_grunt_idle_up->SetVisibility(false, false);

	/********************Punch Left***************************/
	m_grunt_punch_left = CreateDefaultSubobject<UPaperFlipbookComponent>(TEXT("GruntPunchLeftVisual"));
	//m_grunt_punch_left->AttachTo(GetCapsuleComponent());
	m_grunt_punch_left->AttachParent = GetCapsuleComponent();
	m_grunt_punch_left->RelativeLocation = FVector(0.0f, 0.0f, 0.0f);
	m_grunt_punch_left->SetFlipbook(ConstructorHelpers::FObjectFinder<UPaperFlipbook>
	(TEXT("/Game/Animations/Grunt/grunt_punch_left.grunt_punch_left")).Object);
	m_grunt_punch_left->Stop();
	m_grunt_punch_left->SetVisibility(false, false);

	/********************Punch Right***************************/
	m_grunt_punch_right = CreateDefaultSubobject<UPaperFlipbookComponent>(TEXT("GruntPunchRightVisual"));
	m_grunt_punch_right->AttachParent = GetCapsuleComponent();
	m_grunt_punch_right->RelativeLocation = FVector(0.0f, 0.0f, 0.0f);
	//m_grunt_punch_right->RelativeScale3D = FVector(1.5f, 1.5f, 1.5f);
	m_grunt_punch_right->SetFlipbook(ConstructorHelpers::FObjectFinder<UPaperFlipbook>
	(TEXT("/Game/Animations/Grunt/grunt_punch_right.grunt_punch_right")).Object);
	m_grunt_punch_right->Stop();
	m_grunt_punch_right->SetVisibility(false, false);

	/********************Punch Up***************************/
	m_grunt_punch_up = CreateDefaultSubobject<UPaperFlipbookComponent>(TEXT("GruntPunchUpVisual"));
	//m_grunt_punch_up->AttachTo(GetCapsuleComponent());
	m_grunt_punch_up->AttachParent = GetCapsuleComponent();
	m_grunt_punch_up->RelativeLocation = FVector(0.0f, 0.0f, 0.0f);
	//m_grunt_punch_right->RelativeScale3D = FVector(1.5f, 1.5f, 1.5f);
	m_grunt_punch_up->SetFlipbook(ConstructorHelpers::FObjectFinder<UPaperFlipbook>
		(TEXT("/Game/Animations/Grunt/grunt_punch_up.grunt_punch_up")).Object);
	m_grunt_punch_up->Stop();
	m_grunt_punch_up->SetVisibility(false, false);

	/********************Punch Down***************************/
	m_grunt_punch_down = CreateDefaultSubobject<UPaperFlipbookComponent>(TEXT("GruntPunchDownVisual"));
	//m_grunt_punch_down->AttachTo(GetCapsuleComponent());
	m_grunt_punch_down->AttachParent = GetCapsuleComponent();
	m_grunt_punch_down->RelativeLocation = FVector(0.0f, 0.0f, 0.0f);
	//m_grunt_punch_right->RelativeScale3D = FVector(1.5f, 1.5f, 1.5f);
	m_grunt_punch_down->SetFlipbook(ConstructorHelpers::FObjectFinder<UPaperFlipbook>
		(TEXT("/Game/Animations/Grunt/grunt_punch_down.grunt_punch_down")).Object);
	m_grunt_punch_down->Stop();
	m_grunt_punch_down->SetVisibility(false, false);

	/********************Walk Left***************************/
	m_grunt_walk_left = CreateDefaultSubobject<UPaperFlipbookComponent>(TEXT("GruntWalkLeftVisual"));
	//m_grunt_walk_left->AttachTo(GetCapsuleComponent());
	m_grunt_walk_left->AttachParent = GetCapsuleComponent();
	m_grunt_walk_left->RelativeLocation = FVector(0.0f, 0.0f, 0.0f);
	m_grunt_walk_left->SetFlipbook(ConstructorHelpers::FObjectFinder<UPaperFlipbook>
	(TEXT("/Game/Animations/Grunt/grunt_walk_left.grunt_walk_left")).Object);
	m_grunt_walk_left->Stop();
	m_grunt_walk_left->SetVisibility(false, false);

	/********************Walk Right***************************/
	m_grunt_walk_right = CreateDefaultSubobject<UPaperFlipbookComponent>(TEXT("GruntWalkRightVisual"));
	//m_grunt_walk_right->AttachTo(GetCapsuleComponent());
	m_grunt_walk_right->AttachParent = GetCapsuleComponent();
	m_grunt_walk_right->RelativeLocation = FVector(0.0f, 0.0f, 0.0f);
	m_grunt_walk_right->SetFlipbook(ConstructorHelpers::FObjectFinder<UPaperFlipbook>
	(TEXT("/Game/Animations/Grunt/grunt_walk_right.grunt_walk_right")).Object);
	m_grunt_walk_right->Stop();
	m_grunt_walk_right->SetVisibility(false, false);

	/********************Walk Up***************************/
	m_grunt_walk_up = CreateDefaultSubobject<UPaperFlipbookComponent>(TEXT("GruntWalkUpVisual"));
	//m_grunt_walk_up->AttachTo(GetCapsuleComponent());
	m_grunt_walk_up->AttachParent = GetCapsuleComponent();
	m_grunt_walk_up->RelativeLocation = FVector(0.0f, 0.0f, 0.0f);
	m_grunt_walk_up->SetFlipbook(ConstructorHelpers::FObjectFinder<UPaperFlipbook>
		(TEXT("/Game/Animations/Grunt/grunt_walk_up.grunt_walk_up")).Object);
	m_grunt_walk_up->Stop();
	m_grunt_walk_up->SetVisibility(false, false);

	/********************Walk Down***************************/
	m_grunt_walk_down = CreateDefaultSubobject<UPaperFlipbookComponent>(TEXT("GruntWalkDownVisual"));
	//m_grunt_walk_down->AttachTo(GetCapsuleComponent());
	m_grunt_walk_down->AttachParent = GetCapsuleComponent();
	m_grunt_walk_down->RelativeLocation = FVector(0.0f, 0.0f, 0.0f);
	m_grunt_walk_down->SetFlipbook(ConstructorHelpers::FObjectFinder<UPaperFlipbook>
		(TEXT("/Game/Animations/Grunt/grunt_walk_down.grunt_walk_down")).Object);
	m_grunt_walk_down->Stop();
	m_grunt_walk_down->SetVisibility(false, false);

	/********************Grunt Explosion*********************/
	m_grunt_explosion = CreateDefaultSubobject<UPaperFlipbookComponent>(TEXT("GruntExplosionVisual"));
	//m_grunt_explosion->AttachTo(GetCapsuleComponent());
	m_grunt_explosion->AttachParent = GetCapsuleComponent();
	m_grunt_explosion->RelativeLocation = FVector(0.0f, 0.0f, 0.0f);
	m_grunt_explosion->SetFlipbook(ConstructorHelpers::FObjectFinder<UPaperFlipbook>
		(TEXT("/Game/Sprites/explosionFlipbook.ExplosionFlipbook")).Object);
	m_grunt_explosion->Stop();
	m_grunt_explosion->SetVisibility(false, false);


	/********************Attack Range***************************/
	attackRange = CreateDefaultSubobject<USphereComponent>(TEXT("AttackRange"));
	//attackRange->AttachTo(GetRootComponent());
	attackRange->AttachParent = GetCapsuleComponent();


	/********************Chase Range***************************/
	chaseRange = CreateDefaultSubobject<USphereComponent>(TEXT("ChaseRange"));
	//chaseRange->AttachTo(GetRootComponent());
	chaseRange->AttachParent = GetCapsuleComponent();


}

// Called when the game starts or when spawned
void AGruntCharacter::BeginPlay()
{
	Super::BeginPlay();
	if (!player) {
		for (TActorIterator<APlayerPawn> ActorItr(GetWorld()); ActorItr; ++ActorItr) {
			APlayerPawn *Mesh = *ActorItr;
			if (ActorItr->GetName().Contains("Player"))
				player = *ActorItr;
		}
	}

	if(patrol_node_1)
		nextLocation = patrol_node_1->GetActorLocation();

	if (currentFlipbook) {
		currentFlipbook->SetVisibility(true, false);
		currentFlipbook->Play();
	}
	gruntState = GruntState::is_patroling;
}

// Called every frame
void AGruntCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	FVector locked = GetActorLocation();
	//if (isDead) {
		//DestroyGrunt(this);
		//currentFlipbook->Deactivate();
		//SetActorLocation(FVector(GetActorLocation().X, GetActorLocation().Y, -800.0f));
	//}
	if(patrol_node_1)
		SetActorLocation(FVector(locked.X, locked.Y, patrol_node_1->GetActorLocation().Z));
	if (Health <= 0.0f) {
		
		if (currentFlipbook) {
			currentFlipbook->Stop();
			currentFlipbook->SetVisibility(false, false);

			currentFlipbook = m_grunt_explosion;
			currentFlipbook->SetVisibility(true, false);
			currentFlipbook->SetLooping(false);
			//SetLifeSpan(currentFlipbook->GetFlipbookLength());
			currentFlipbook->Play();

			gruntState = GruntState::is_dead;
			
			
			//DestroyGrunt(this);
			//GetWorld()->RemoveActor(this, true);
			//Destroy();
			
		}
		
		//set current flipbook to the death animation
		else {
			
			//summoning rama
			DestroyGrunt(this);
		}
		
	}
	else {
			AnimStateMachine(gruntState, directionFacing);
			GruntStateMachine(gruntState);
	}
}

// Called to bind functionality to input
void AGruntCharacter::SetupPlayerInputComponent(class UInputComponent* InputComponent)
{
	Super::SetupPlayerInputComponent(InputComponent);

}

//initial state machine for the grunt's behavior
void AGruntCharacter::GruntStateMachine(GruntState grunt_state) {
	FTimerHandle timer;

	switch (gruntState) {

	case GruntState::is_patroling:

		PatrolPath(patrol_node_1, patrol_node_2);
		MoveToLocation(nextLocation);
		break;

	case GruntState::is_chasing:

		if((player != nullptr) && (currentFlipbook != nullptr))
			nextLocation = FMath::VInterpTo(GetActorLocation(), player->GetActorLocation(), GetWorld()->DeltaTimeSeconds, currentFlipbook->GetFlipbookLength());

		MoveToLocation(nextLocation);
		break;

	case GruntState::is_punching:
		//set a timer for the length of the animation, then move out of range until is chasing is true
		GetWorldTimerManager().SetTimer(timer, currentFlipbook->GetFlipbookLength(), false, -1.0f);
		break;

	case GruntState::is_dead:
		//DestroyGrunt(this);
		currentFlipbook = m_grunt_explosion;
		currentFlipbook->SetVisibility(true, false);
		currentFlipbook->SetLooping(false);
		//SetLifeSpan(currentFlipbook->GetFlipbookLength());
		currentFlipbook->Play();
		if (!currentFlipbook->IsPlaying()) {
			DestroyGrunt(this);
		}
		//SetActorLocation(FVector(GetActorLocation().X, GetActorLocation().Y, -800.0f));
		break;
	}
}

//moves the grunt to the next location
//changes animation based on location
//returns whether the move was successful or not
bool AGruntCharacter::MoveToLocation(FVector new_location) {
	Super::Tick(GetWorld()->GetDeltaSeconds());
	//check orientation first
	/***********************Facing Right*****************************/
	if (GetActorLocation().X < new_location.X) {
		directionFacing = DirectionFacing::right;
	}
	/***********************Facing Left*****************************/
	else if (GetActorLocation().X > new_location.X) {
		directionFacing = DirectionFacing::left;
	}

	/***********************Facing Down*****************************/
	else if ((GetActorLocation().X == new_location.X) && (GetActorLocation().Y > new_location.Y)) {
		directionFacing = DirectionFacing::down;
	}

	/***********************Facing Up*****************************/
	else if ((GetActorLocation().X == new_location.X) && (GetActorLocation().Y < new_location.Y)) {
		directionFacing = DirectionFacing::up;
	}

	//set new location to be the next location of the grunt
	//do this based on distance to the location
	float dist_to_new_loc = FVector::Dist(GetActorLocation(), new_location);
	if (dist_to_new_loc > 1.0f && !hasArrived) {
		FHitResult HitResult;
		nextLocation = new_location;
		if (currentFlipbook) {
			FVector interpPos = FMath::InterpEaseIn(GetActorLocation(), nextLocation, GetWorld()->GetDeltaSeconds(), currentFlipbook->GetFlipbookLength());
			SetActorLocation(interpPos);
		}
		
		float dist_to_location = FVector::Dist(GetActorLocation(), nextLocation);
		if (dist_to_location <= 1.0f && !hasArrived) {
			hasArrived = true;
			return true;
		}
		if(hasArrived)
		return true;
	}
	//failure
	else if(!hasArrived)
		return false;
	
	return false;
}

//patrols between two paths
//changes animations based on the location of the next location of the grunt
void AGruntCharacter::PatrolPath(APathNodeActor* patrol1, APathNodeActor* patrol2) {
	//float dist_to_node_1, dist_to_node_2;

	
	if(patrol1 && patrol2)
	if (is_patroling) {
		//dist_to_node_1 = FVector::Dist(GetActorLocation(), patrol1->GetActorLocation());
		if (hasArrived && nextLocation == patrol1->GetActorLocation()) {
			hasArrived = false;
			nextLocation = patrol2->GetActorLocation();
		}
		else if (hasArrived && nextLocation == patrol2->GetActorLocation()) {
			hasArrived = false;
			nextLocation = patrol1->GetActorLocation();
		}

	}
}

//attacks the player
void AGruntCharacter::Attack() {
	if (is_attacking) {
		switch (directionFacing) {

			case DirectionFacing::up:
				GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red,
					TEXT("Attacking Up")); 
				hasArrived = false;

				break;
			case DirectionFacing::down:
				GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red,
					TEXT("Attacking Down"));
				hasArrived = false;
				break;
			case DirectionFacing::right:
				GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red,
					TEXT("Attacking Right"));
				hasArrived = false;
				break;
			case DirectionFacing::left:
				GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red,
					TEXT("Attacking Left"));
				hasArrived = false;
				break;

		}
	}
}
/****************************************************Grunt Destruction******************************************************/
/***************************************************************************************************************************/
/***************************************************************************************************************************/
/***************************************************************************************************************************/
//Author: Marco Duran and Rama from the Unreal Engine Forums****************************************************************/
//URL: https://answers.unrealengine.com/questions/3953/question-how-to-destroy-an-actor-properly-in-c.html *****************/
/*Description: Plays the grunt destruction animation and then properly removes the grunt from the world*********************/
/***************************************************************************************************************************/
void AGruntCharacter::DestroyGrunt(UObject* toDestroy) {
	FTimerHandle timer;
	if (!toDestroy) return;
	if (!toDestroy->IsValidLowLevel()) return;
	
	//Actor?
	AActor * anActor = Cast<AActor>(toDestroy);
	if (anActor) {
		
		//if (!currentFlipbook->IsPlaying()) {
			anActor->K2_DestroyActor();

			toDestroy = NULL;
		//}
		//currentFlipbook->OnFinishedPlaying;
	}

	//object begin destroy
	else {
		//begin destroy
		toDestroy->ConditionalBeginDestroy();
		toDestroy = NULL;
	}

	//GC
	GetWorld()->ForceGarbageCollection(true);

}

/*****************************************Animation State Machine*******************************************************/
/***********************************************************************************************************************/
/***********************************************************************************************************************/
/******Author: Marco Duran *********************************************************************************************/
/*****Description: This is a state machine that checks both the state of the grunt and the direction in order to play **/
/****************  the correct animation *******************************************************************************/
/***********************************************************************************************************************/
void AGruntCharacter::AnimStateMachine(GruntState grunt_state, DirectionFacing direction) {
	switch (grunt_state) {
	case GruntState::is_chasing:
		switch (direction) {
		case DirectionFacing::up:
			//stop currentFlipbook
			if (currentFlipbook) {
				currentFlipbook->Stop();
				currentFlipbook->SetVisibility(false, false);
			}
			//set new flipbook
			currentFlipbook = m_grunt_walk_up;
			//set visible
			currentFlipbook->SetVisibility(true, false);
			//play flipbook
			currentFlipbook->Play();
			break;
		case DirectionFacing::down:
			//stop currentFlipbook
			if (currentFlipbook) {
				currentFlipbook->Stop();
				currentFlipbook->SetVisibility(false, false);
			}
			//set new flipbook
			currentFlipbook = m_grunt_walk_down;
			//set visible
			currentFlipbook->SetVisibility(true, false);
			//play flipbook
			currentFlipbook->Play();
			break;
		case DirectionFacing::left:
			//stop currentFlipbook
			if (currentFlipbook) {
				currentFlipbook->Stop();
				currentFlipbook->SetVisibility(false, false);
			}
			//set new flipbook
			currentFlipbook = m_grunt_walk_left;
			//set visible
			currentFlipbook->SetVisibility(true, false);
			//play flipbook
			currentFlipbook->Play();
			break;
		case DirectionFacing::right:
			//stop currentFlipbook
			if (currentFlipbook) {
				currentFlipbook->Stop();
				currentFlipbook->SetVisibility(false, false);
			}
			//set new flipbook
			currentFlipbook = m_grunt_walk_right;
			//set visible
			currentFlipbook->SetVisibility(true, false);
			//play flipbook
			currentFlipbook->Play();
			break;
		default:
			//stop currentFlipbook
			if (currentFlipbook) {
				currentFlipbook->Stop();
				currentFlipbook->SetVisibility(false, false);
			}
			//set new flipbook
			currentFlipbook = m_grunt_walk_right;
			//set visible
			currentFlipbook->SetVisibility(true, false);
			//play flipbook
			currentFlipbook->Play();
			break;
		}
		break;
	case GruntState::is_patroling:
		switch (direction) {
		case DirectionFacing::up:
			//stop currentFlipbook
			if (currentFlipbook) {
				currentFlipbook->Stop();
				currentFlipbook->SetVisibility(false, false);
			}
			//set new flipbook
			currentFlipbook = m_grunt_walk_up;
			//set visible
			currentFlipbook->SetVisibility(true, false);
			//play flipbook
			currentFlipbook->Play();
			break;
		case DirectionFacing::down:
			//stop currentFlipbook
			if (currentFlipbook) {
				currentFlipbook->Stop();
				currentFlipbook->SetVisibility(false, false);
			}
			//set new flipbook
			currentFlipbook = m_grunt_walk_down;
			//set visible
			currentFlipbook->SetVisibility(true, false);
			//play flipbook
			currentFlipbook->Play();
			break;
		case DirectionFacing::left:
			//stop currentFlipbook
			if (currentFlipbook) {
				currentFlipbook->Stop();
				currentFlipbook->SetVisibility(false, false);
			}
			//set new flipbook
			currentFlipbook = m_grunt_walk_left;
			//set visible
			currentFlipbook->SetVisibility(true, false);
			//play flipbook
			currentFlipbook->Play();
			break;
		case DirectionFacing::right:
			//stop currentFlipbook
			if (currentFlipbook) {
				currentFlipbook->Stop();
				currentFlipbook->SetVisibility(false, false);
			}
			//set new flipbook
			currentFlipbook = m_grunt_walk_right;
			//set visible
			currentFlipbook->SetVisibility(true, false);
			//play flipbook
			currentFlipbook->Play();
			break;
		default:
			//stop currentFlipbook
			if (currentFlipbook) {
				currentFlipbook->Stop();
				currentFlipbook->SetVisibility(false, false);
			}
			//set new flipbook
			currentFlipbook = m_grunt_walk_right;
			//set visible
			currentFlipbook->SetVisibility(true, false);
			//play flipbook
			currentFlipbook->Play();
			break;
		}

		break;
	case GruntState::is_punching:
		switch (direction) {
		case DirectionFacing::up:
			//stop currentFlipbook
			if (currentFlipbook) {
				currentFlipbook->Stop();
				currentFlipbook->SetVisibility(false, false);
			}
			//set new flipbook
			currentFlipbook = m_grunt_punch_up;
			//set visible
			currentFlipbook->SetVisibility(true, false);
			//play flipbook
			currentFlipbook->Play();
			break;
		case DirectionFacing::down:
			//stop currentFlipbook
			if (currentFlipbook) {
				currentFlipbook->Stop();
				currentFlipbook->SetVisibility(false, false);
			}
			//set new flipbook
			currentFlipbook = m_grunt_punch_down;
			//set visible
			currentFlipbook->SetVisibility(true, false);
			//play flipbook
			currentFlipbook->Play();
			break;
		case DirectionFacing::left:
			//stop currentFlipbook
			if (currentFlipbook) {
				currentFlipbook->Stop();
				currentFlipbook->SetVisibility(false, false);
			}
			//set new flipbook
			currentFlipbook = m_grunt_punch_left;
			//set visible
			currentFlipbook->SetVisibility(true, false);
			//play flipbook
			currentFlipbook->Play();
			break;
		case DirectionFacing::right:
			//stop currentFlipbook
			if (currentFlipbook) {
				currentFlipbook->Stop();
				currentFlipbook->SetVisibility(false, false);
			}
			//set new flipbook
			currentFlipbook = m_grunt_punch_right;
			//set visible
			currentFlipbook->SetVisibility(true, false);
			//play flipbook
			currentFlipbook->Play();
			break;
		default:
			//stop currentFlipbook
			if (currentFlipbook) {
				currentFlipbook->Stop();
				currentFlipbook->SetVisibility(false, false);
			}
			//set new flipbook
			currentFlipbook = m_grunt_punch_right;
			//set visible
			currentFlipbook->SetVisibility(true, false);
			//play flipbook
			currentFlipbook->Play();
			break;
		}
		break;
	default:
		switch (direction) {
		case DirectionFacing::up:
			//stop currentFlipbook
			if (currentFlipbook) {
				currentFlipbook->Stop();
				currentFlipbook->SetVisibility(false, false);
			}
			//set new flipbook
			currentFlipbook = m_grunt_idle_up;
			//set visible
			currentFlipbook->SetVisibility(true, false);
			//play flipbook
			currentFlipbook->Play();
			break;
		case DirectionFacing::down:
			//stop currentFlipbook
			if (currentFlipbook) {
				currentFlipbook->Stop();
				currentFlipbook->SetVisibility(false, false);
			}
			//set new flipbook
			currentFlipbook = m_grunt_idle_down;
			//set visible
			currentFlipbook->SetVisibility(true, false);
			//play flipbook
			currentFlipbook->Play();
			break;
		case DirectionFacing::left:
			//stop currentFlipbook
			if (currentFlipbook) {
				currentFlipbook->Stop();
				currentFlipbook->SetVisibility(false, false);
			}
			//set new flipbook
			currentFlipbook = m_grunt_idle_left;
			//set visible
			currentFlipbook->SetVisibility(true, false);
			//play flipbook
			currentFlipbook->Play();
			break;
		case DirectionFacing::right:
			//stop currentFlipbook
			if (currentFlipbook) {
				currentFlipbook->Stop();
				currentFlipbook->SetVisibility(false, false);
			}
			//set new flipbook
			currentFlipbook = m_grunt_idle_right;
			//set visible
			currentFlipbook->SetVisibility(true, false);
			//play flipbook
			currentFlipbook->Play();
			break;
		default:
			//stop currentFlipbook
			if (currentFlipbook) {
				currentFlipbook->Stop();
				currentFlipbook->SetVisibility(false, false);
			}
			//set new flipbook
			currentFlipbook = m_grunt_idle_right;
			//set visible
			currentFlipbook->SetVisibility(true, false);
			//play flipbook
			currentFlipbook->Play();
			break;
		}
		break;
	}
	
}
