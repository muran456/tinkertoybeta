// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Pawn.h"
#include "PaperSpriteComponent.h"
#include "PaperFlipbookComponent.h"
#include "PlayerPawn.generated.h"

UCLASS()
class TINKERTOYTESTBUILD_API APlayerPawn : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	APlayerPawn();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* InputComponent) override;

	UFUNCTION()
		FVector FindClosestNodeStart(FVector fromHere, FVector toThere);

	UFUNCTION()
		FVector FindClosestNode();
	
	UFUNCTION(BlueprintCallable, Category = "MakePathCat")
		void MakePathNode();

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FVector NewDestination;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FVector CurrentProjectedLoc;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		bool destIsSet;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		bool goPath;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		TArray<FVector> NodeLocArray;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		TArray<FVector> CurrentConnectionList;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		TArray<FVector> ourPath;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		bool alreadyMaking = false;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		bool atStart = true;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		bool foundMyHome;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FVector firstLocation;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		bool newPointAdded;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float PlayerHP = 100.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		bool isHit = false;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		bool isAttacking = false;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		UCapsuleComponent* root;
private:
	UPROPERTY(VisibleDefaultsOnly)
		UPaperSpriteComponent *m_personVisual;

	UPROPERTY(VisibleDefaultsOnly)
		UFloatingPawnMovement *m_movementComponent;



};

//General Log
DECLARE_LOG_CATEGORY_EXTERN(YourLog, Log, All);