// Fill out your copyright notice in the Description page of Project Settings.

#include "TinkerToyTestBuild.h"
#include "PathNodeActor.h"
#include "EngineUtils.h"

// Sets default values
APathNodeActor::APathNodeActor()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	// Create the sprite for the ship.
	m_pathVisual = CreateDefaultSubobject<UPaperSpriteComponent>(TEXT("PathVisual"));
	m_pathVisual->RelativeLocation = FVector(0.0f, 0.0f, 0.0f);
	m_pathVisual->SetSprite(ConstructorHelpers::FObjectFinder<UPaperSprite>
		(TEXT("/Game/Sprites/player_waypoint_sprite_Sprite.player_waypoint_sprite_Sprite")).Object);

	RootComponent = m_pathVisual;


}

// Called when the game starts or when spawned
void APathNodeActor::BeginPlay()
{
	Super::BeginPlay();



	FVector ourLocation = GetActorLocation();



	if (GetWorld()) {
		// Iterate over all Actors in the world.
		for (TActorIterator<AActor> ActorItr(GetWorld()); ActorItr; ++ActorItr) {
			AActor *Mesh = *ActorItr;
			// If Actor is a �PathNode�, display location on screen.
			if (ActorItr->GetName().Contains("PathNode_BP")) {
				FVector itsLocation = ActorItr->GetActorLocation();
				float sizeOfAB = (ourLocation - itsLocation).Size();
				//right next to or above/below the other node
				if (sizeOfAB == 200) {
					connectToLocations.Add(itsLocation);
					//GEngine->AddOnScreenDebugMessage(-1, 50.f, FColor::Red, TEXT("Added node location"));

				}
				if (290 > sizeOfAB && sizeOfAB > 200) {
					connectToLocations.Add(itsLocation);
					//GEngine->AddOnScreenDebugMessage(-1, 50.f, FColor::Red, TEXT("Added node location"));
				}
			}
		}
	}
}

// Called every frame
void APathNodeActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

TArray<FVector> APathNodeActor::getConnectToLocations()
{
	return connectToLocations;
}