// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Character.h"
#include "PaperSpriteComponent.h"
#include "PaperFlipbookComponent.h"
#include "PathNodeActor.h"
#include "PlayerPawn.h"
#include "GruntCharacter.generated.h"

UENUM(BlueprintType)
enum class DirectionFacing : uint8 {
	left	UMETA(DisplayName = "Left"),
	right   UMETA(DisplayName = "Right"),
	up		UMETA(DisplayName = "Up"),
	down	UMETA(DisplayName = "Down"),
};

UENUM(BlueprintType)
enum class GruntState : uint8 {
	is_patroling	UMETA(DisplayName = "IsPatroling"),
	is_chasing		UMETA(DisplayName = "IsChasing"),
	is_punching		UMETA(DisplayName = "IsPunching"),
	is_dead			UMETA(DisplayName = "IsDead"),
};

UCLASS()
class TINKERTOYTESTBUILD_API AGruntCharacter : public ACharacter
{
	GENERATED_BODY()


public:
	/********************************Defaults**************************************/
	/******************************************************************************/
	/******************************************************************************/
	// Sets default values for this character's properties
	AGruntCharacter();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	// Called every frame
	virtual void Tick(float DeltaSeconds) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* InputComponent) override;

	/*********************************Properties***********************************/
	/******************************************************************************/
	/******************************************************************************/
	UPROPERTY(VisibleDefaultsOnly, BlueprintReadOnly)
		USphereComponent* collision;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float Health = 30.0f;

	//Direction that the grunt is facing
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category=Enum)
		DirectionFacing directionFacing = DirectionFacing::right;

	//State that the grunt is currently in
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category=Enum)
		GruntState gruntState = GruntState::is_patroling;

	//boolean to check if the grunt is hit
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		bool isHit = false;

	//boolean to check if the grunt is dead
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		bool isDead = false;

	//check to see if the grunt is attacking
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		bool is_attacking = false;

	//check to see if the grunt is chasing
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		bool is_chasing = false;

	//check to see if the grunt is chasing
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		bool is_patroling = true;

	//checks to see if the grunt has arrived at its next location
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		bool hasArrived = false;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		APathNodeActor* patrol_node_1;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		APathNodeActor* patrol_node_2;

	//Attack aggro range
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
		USphereComponent* attackRange;

	//Chase aggro range
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
		USphereComponent* chaseRange;

	//Returns the vector of the next location
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
		FVector nextLocation;

	//Reference to the player character
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		APlayerPawn* player;

	//Shows the flipbook that is currently in use
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		UPaperFlipbookComponent* currentFlipbook;

	//***********************************************************************
	//**************************Functions************************************
	//***********************************************************************
	//***********************************************************************
	UFUNCTION()
		void GruntStateMachine(GruntState grunt_state);

	UFUNCTION()
		void PatrolPath(APathNodeActor* patrol1, APathNodeActor* patrol2);

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Pathfinding")
		bool MoveToLocation(FVector new_location);

	//destroy those little grunts
	UFUNCTION()
		void DestroyGrunt(UObject* toDestroy);

	UFUNCTION()
		void Attack();

	UFUNCTION()
		void AnimStateMachine(GruntState grunt_state, DirectionFacing direction);

private:
	UPROPERTY(VisibleDefaultsOnly)
		UPaperFlipbookComponent* m_grunt_idle_left;
	UPROPERTY(VisibleDefaultsOnly)
		UPaperFlipbookComponent* m_grunt_idle_right;
	UPROPERTY(VisibleDefaultsOnly)
		UPaperFlipbookComponent* m_grunt_idle_up;
	UPROPERTY(VisibleDefaultsOnly)
		UPaperFlipbookComponent* m_grunt_idle_down;

	UPROPERTY(VisibleDefaultsOnly)
		UPaperFlipbookComponent* m_grunt_punch_left;
	UPROPERTY(VisibleDefaultsOnly)
		UPaperFlipbookComponent* m_grunt_punch_right;
	UPROPERTY(VisibleDefaultsOnly)
		UPaperFlipbookComponent* m_grunt_punch_up;
	UPROPERTY(VisibleDefaultsOnly)
		UPaperFlipbookComponent* m_grunt_punch_down;

	//walking animations
	UPROPERTY(VisibleDefaultsOnly)
		UPaperFlipbookComponent* m_grunt_walk_left;
	UPROPERTY(VisibleDefaultsOnly)
		UPaperFlipbookComponent* m_grunt_walk_right;
	UPROPERTY(VisibleDefaultsOnly)
		UPaperFlipbookComponent* m_grunt_walk_up;
	UPROPERTY(VisibleDefaultsOnly)
		UPaperFlipbookComponent* m_grunt_walk_down;
	
	//explosion animation
	UPROPERTY(VisibleDefaultsOnly)
		UPaperFlipbookComponent* m_grunt_explosion;
};